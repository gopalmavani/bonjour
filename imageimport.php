<?php
require_once 'app/Mage.php';
Mage::app();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$collection = Mage::getResourceModel('catalog/product_collection')
    ->setPageSize(100)
    ->setCurPage(53);
$count = 0;
foreach ($collection as $c){
$img_path = array();
    $sku = $c->getSku();
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://bonjour-promo.com/test.php?sku=".$sku,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo 'count'.++$count.'######'.$sku.$response;
        $pro_data = json_decode($response, true);

        foreach ($pro_data['images'] as $key => $img) {
            $path = explode("/", $img); // splitting the path
            $image_url = end($path);
            $image_name = explode('.',$image_url)[0];
            $image_type = substr(strrchr($image_url, "."), 1); //find the image extension
            $filename = $image_name.'.'. $image_type; //give a new name, you can modify as per your requirement
            $filepath = Mage::getBaseDir('media') . DS . 'import' . DS . $filename; //path for temp storage folder: ./media/import/
            file_put_contents($filepath, file_get_contents($img)); //store the image from external url to the temp storage folder
            $img_path[] = $filepath;
        }
    }
    $product_id = Mage::getModel("catalog/product")->getIdBySku($sku);
    $model = Mage::getModel('catalog/product')->load($product_id);
    if ($img_path) {
        foreach ($img_path as $path) {
            echo $path;
            try {
                $model->addImageToMediaGallery($path, array('image', 'thumbnail', 'small_image'), false);
            } catch (Exception $e) {
                //continue;
                echo $e->getMessage();
            }
        }
    }
    $model->save();
    echo 'save';
}



