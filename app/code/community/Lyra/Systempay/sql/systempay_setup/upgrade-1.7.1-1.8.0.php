<?php
/**
 * Systempay V2-Payment Module version 1.8.0 for Magento 1.4-1.9. Support contact : supportvad@lyra-network.com.
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 *
 * @author    Lyra Network (http://www.lyra-network.com/)
 * @copyright 2014-2017 Lyra Network and contributors
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @category  payment
 * @package   systempay
 */

/**
 * This file is recognized by 1.6 and up Magento versions.
 */

/** @var $this Lyra_Systempay_Model_Resource_Setup */
$installer = $this;

/** @var $installer Mage_Paypal_Model_Resource_Setup */
$installer->addAttribute('customer', 'systempay_masked_card', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Systempay masked card',

    'global' => 1,
    'visible' => 0,
    'searchable' => 0,
    'filterable' => 0,
    'comparable' => 0,
    'visible_on_front' => 0,
    'required' => 0,
    'user_defined' => 0,
    'default' => '',
    'source' => null
));

$entityTypeId     = $installer->getEntityTypeId('customer');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'systempay_masked_card',
    '999' // sort_order
);
