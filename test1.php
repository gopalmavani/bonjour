<?php
/**
* Script created by sonassi.com (http://www.sonassi.com/knowledge-base/quick-script-batch-create-magento-categories/)
*
* Edited by Christer Johansson for Magento 1.9.2.2 in december 2015
*
* File format of the CSV file categories-and-ids.csv :
* parent_category_id,category_name,category_id
* example: 3,subcat,5
* -> This will create a subcategory with 'subcat' as name and 5 as category id. The parent category id is 3 (Can also be Root
* Category with id 0).
*/

// define('MAGENTO', realpath(dirname(__FILE__)));
// require_once MAGENTO . '/app/Mage.php';

// setlocale(LC_ALL, 'en_US.UTF-8');
// umask(0);
// Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// $file = fopen('category_export.csv', 'r');
// try{
//         while (($column = fgetcsv($file)) !== FALSE) {
//                 //$column is an array of the csv elements
//                 if (!empty($column[0]) && !empty($column[1]) && !empty($column[2])) {
//                         $data['general']['name'] = $column[1];
//                         $data['general']['entity_id'] = $column[0];
//                         $data['general']['meta_title'] = "";
//                         $data['general']['meta_description'] = "";
//                         $data['general']['is_active'] = 1;
//                         $data['general']['url_key'] =  $column[2];
//                         $data['general']['display_mode'] = "PRODUCTS";
//                         $data['general']['is_anchor'] = 0;

//                         $data['category']['parent'] = $column[4]; // 2 or 3 is top level depending on Magento version
//                         $storeId = 0;
//                         // print_r(Mage::app()->getStore()->getStoreId());die;

//                         createCategory($data, $storeId);
//                         sleep(0.5);
//                         unset($data);
//                 }
//         }
// }catch(Exception $e){
//         print($e);
// }
// function createCategory($data, $storeId) {
//         try{
//                 // echo "Starting {$data['general']['name']} [{$data['category']['parent']}] ...";

//                 $category = Mage::getModel('catalog/category');
//                 $category->setStoreId($storeId);
// //                 echo '<pre>';
// // print_r($data);die;
//                 if (is_array($data)) {
//                         $category->addData($data['general']);

//                         $parentId = $data['category']['parent'];
//                         $parentCategory = Mage::getModel('catalog/category')->load($parentId);
//                         $category->setPath($parentCategory->getPath() . "/" . $category->getId());

//                         /**
//                         * Check "Use Default Value" checkboxes values
//                         */
//                         if ($useDefaults = $data['use_default']) {
//                                 foreach ($useDefaults as $attributeCode) {
//                                         $category->setData($attributeCode, null);
//                                 }
//                         }

//                         $category->setAttributeSetId($category->getDefaultAttributeSetId());

//                         if (isset($data['category_products']) && !$category->getProductsReadonly()) {
//                                 $products = [];
//                                 parse_str($data['category_products'], $products);
//                                 $category->setPostedProducts($products);
//                         }

//                         try {
//                                 $category->save();
//                                 echo "Import successful - ID: " . $category->getId() . " - " . $category->getPath() . "<br /> ";
//                         } catch (Exception $e){
//                                 echo "Failed import <br />";
//                         }
//                 }

//         }catch(Exception $e){
//                 print_r($e);
//         }
                
// }      

// require_once dirname(__FILE__) . '/app/Mage.php';
// Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
 
// $resource = Mage::getSingleton('core/resource');
// $db_read = $resource->getConnection('core_read');
 
// $categories = $db_read->fetchCol("SELECT entity_id FROM " . $resource->getTableName("catalog_category_entity") . " WHERE entity_id>1 ORDER BY entity_id DESC");
// foreach ($categories as $category_id) {
//     try {
//         Mage::getModel("catalog/category")->load($category_id)->delete();
//     } catch (Exception $e) {
//         echo $e->getMessage() . "\n";
//     }
// }
